import dash
import dash_table
import pandas as pd

df = pd.read_csv('https://api.covid19india.org/csv/latest/state_wise.csv') # covid 19 dataset state wise.
df['New_Active_Cases'] = df['Delta_Confirmed'] - df['Delta_Recovered'] - df['Delta_Deaths']
 # above Calculates daily new active coronavirus cases.
print(df['New_Active_Cases'].head(5)) # Calculates daily new active coronavirus cases.
bootstrap = "https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/solar/bootstrap.min.css"
app = dash.Dash(__name__, external_stylesheets=[bootstrap])

app.layout = dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in df.columns],
    data=df.to_dict('records')
)

if __name__ == '__main__':
    app.run_server(debug=True)
